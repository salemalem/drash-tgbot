import * as Drash from "https://deno.land/x/drash@v2.5.4/mod.ts";

class HomeResource extends Drash.Resource {
  // paths = ["/bot5363108096:AAFv-9ymHAiMqCJqLmkQisY24xCF-tDbT5o"];
  paths = ["/"];

  GET(request, response) {
    console.log(request);
    return response.json({
      hello: "world",
      time: new Date(),
    });
  }

  async POST(request) {
    console.log(request);
    let message = request.bodyAll().message;
    if (message.from.id == "5105596720") {
      if (message.reply_to_message == null) {
        await fetch("https://api.telegram.org/bot5363108096:AAFv-9ymHAiMqCJqLmkQisY24xCF-tDbT5o/sendMessage?chat_id=5105596720&text='Please, choose a message and reply to it'");
      } else {
        let receiver_chat_id = message.reply_to_message.chat.id;
        await fetch("https://api.telegram.org/bot5363108096:AAFv-9ymHAiMqCJqLmkQisY24xCF-tDbT5o/sendMessage?chat_id="+ receiver_chat_id + "&text='Please, choose a message and reply to it'");
      }
    } else {
      await fetch("https://api.telegram.org/bot5363108096:AAFv-9ymHAiMqCJqLmkQisY24xCF-tDbT5o/sendMessage?chat_id=5105596720&text='Text from stranger'");
    }
  }
}

const server = new Drash.Server({
  hostname: "",
  port: 8443,
  protocol: "https",
  resources: [HomeResource],
  cert_file: "./protocol3.pem",
  key_file:  "./protocol3.key",
});

server.run();

console.log("Server is running");
